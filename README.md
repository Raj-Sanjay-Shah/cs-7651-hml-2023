# cs-7651-hml-2023


# CS 7651: Human and Machine Learning

* [Assignment 1](assignment-1-decision-trees.ipynb)
  (`assignment-1-decision-trees.ipynb`)

* [Assignment 2](assignment_2_prototype_and_exemplar_models.ipynb)
  (`assignment_2_prototype_and_exemplar_models.ipynb`)

* [Assignment 3](assignment-3-vector-representations-and-lsa.ipynb)
  (`assignment-3-vector-representations-and-lsa.ipynb`)

* [Assignment 4](assignment-4-neural-networks.ipynb)
  (`assignment-4-neural-networks.ipynb`)

* [Assignment 5](assignment-5-levels-of-processing.ipynb)
  (`assignment-5-levels-of-processing.ipynb`)

* [Assignment 6](assignment-6-numerosity.ipynb)
  (`assignment-6-numerosity.ipynb`)

* [Assignment 7](assignment-7-logistic-regression-and-k-nearest-neighbors.ipynb)
  (`assignment-7-logistic-regression-and-k-nearest-neighbors.ipynb`)


Use either `git clone
https://gitlab.com/Raj-Sanjay-Shah/cs-7651-hml-2023`, `git pull
https://gitlab.com/Raj-Sanjay-Shah/cs-7651-hml-2023` or the download
icon on near the right (next to clone icon) to download the files and
access the Jupyter notebooks for the assignments.
