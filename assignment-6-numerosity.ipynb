{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9cd7045a",
   "metadata": {
    "id": "9cd7045a"
   },
   "source": [
    "# Assignment 6: Understanding Numerosity in Vision Models\n",
    "\n",
    "**Please do not consult external resources for this assignment.**\n",
    "\n",
    "Make sure you have done the required reading for this homework (which was also required reading for class):\n",
    "\n",
    "- [Testolin, A., Zou, W. Y., & McClelland, J. L. (2020). Numerosity discrimination in deep neural networks: Initial competence, developmental refinement and experience statistics. Developmental Science, 23, e12940.](https://gatech.instructure.com/courses/352188/files?preview=44758895)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28972179",
   "metadata": {
    "id": "28972179"
   },
   "source": [
    "## Summary\n",
    "\n",
    "The goal of this homework is to gain comfort in working with computer vision models and to apply ML models to understand mathematical concepts. \n",
    "\n",
    "In more detail, you will replicate one of the computational experiments conducted by Upadhyay and Varma that was discussed in the lecture on Mathematical Cognition. That experiment investigated whether Convolutional Neural Network (CNN) models, in learning to classify images, also learn “for free” something about numbers. Numbers can be named in various ways, most commonly using digits (e.g., “3”). In this experiment, you will work with ‘numerosities’, or sets of objects (e.g., “o o o”) that can be interpreted as naming the the numbers that are their cardinalities (e.g., 3). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ed4bf17",
   "metadata": {
    "id": "3ed4bf17"
   },
   "source": [
    "\n",
    "\n",
    "![](numerosity.png)\n",
    "\n",
    "Several important effects have been shown when people compare two numerosities and judge which one is greater. Together, they suggest that people understand numbers using ‘magnitude representations’ that are organized as a a Mental Number Line (MNL). This homework will focus on two such effects, the distance effect and the ratio effect. \n",
    "\n",
    "* **Distance effect**: The time for people to compare which of two numbers x and y is greater decreases as the distance |x − y| between them increases (Moyer & Landauer, 1967). This is consistent with the following process model: fixate x and y on the MNL and discriminate which one is ’to the right’.\n",
    "\n",
    "* **Ratio effect**: The time to compare which of two numbers x and y is greater decreases as the ratio of the greater number over the smaller number (i.e., max (x, y) / min(x, y)) increases according to a nonlinear psychophysical function (Halberda et al., 2008).\n",
    "\n",
    "In this homework, you will work with the CNN model VGG19 from the PyTorch model zoo. You will present images of the numerosities 1-9 to the model and read out their vector representations on the final fully connected layer. For each pair of images of differing numerosities n1 and n2, you will compute the cosine similarities of their vector representations, and store these in a matrix. You will then evaluate whether these values show evidence of the distance effect and the size effect.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "608e56a7",
   "metadata": {
    "id": "608e56a7"
   },
   "source": [
    "## Submission guidelines\n",
    "\n",
    "Please upload your Jupyter notebook to the Canvas Assignment. Please do not include any system specific configuration, such as the installation of dependencies, as code in the notebook (you can comment it out)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4da140b3",
   "metadata": {},
   "source": [
    "## Required Downloads\n",
    "\n",
    "Download Numerosity_dataset.zip, numerosity.png, expected_distance_effect.png, expected_ratio_effect.png, and children_distance_effect.png from the files/ folder in the gitlab "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d73f6cc",
   "metadata": {},
   "source": [
    "## Install Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd058a60",
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install torch\n",
    "!pip install Pillow\n",
    "!unzip Numerosity_dataset.zip"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3982a363",
   "metadata": {
    "id": "3982a363"
   },
   "source": [
    "## Load pretrained VGG19 Model\n",
    "\n",
    "VGG19 is a deep Convolution Neural Network developed at the University of Oxford by the Visual Geometry Group (VGG).   \n",
    "\n",
    "There are various pretrained models available for VGG19 which are trained on different set of images. VGG19 pretrained models are mostly used after fine-tuning over some downstream vision tasks using transfer learning. However, in this assignment we will not be retraining or fine-tuning the model, we will just be analyzing the output from the final activation layer from the model.\n",
    "\n",
    "In the below cell, we will be loading the pretrained VGG19 model provided by pytorch. We have provided the code for it. Refer here for more details about the pretrained VGG19 model: [[Pytorch Pretrained VGG19]](https://pytorch.org/vision/main/models/generated/torchvision.models.vgg19.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a075857",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "6a075857",
    "outputId": "5fac6eef-3fe3-476c-9305-b954926fdde8"
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "\n",
    "def load_model(model_name):\n",
    "    if model_name==\"vgg19\":\n",
    "        model = torch.hub.load('pytorch/vision:v0.10.0', 'vgg19', pretrained=True)\n",
    "    return model\n",
    "\n",
    "device = 'cuda' if torch.cuda.is_available() else 'cpu'\n",
    "model = load_model(\"vgg19\").to(device)\n",
    "model.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f22cd891",
   "metadata": {
    "id": "f22cd891"
   },
   "source": [
    "## Loading the numerosity stimuli"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d02cbcd",
   "metadata": {
    "id": "1d02cbcd"
   },
   "source": [
    "\n",
    "The function defined by us in the below cell will load all the images for a particular numerosity N (1-9).\n",
    "\n",
    "If you notice, after loading images we perform some pre-processing over the input images. We are first resizing the images to 256x256 (images in the dataset are 1000x1000), and then normalizing it using the mean and standard deviation of the images over which the VGG19 was pretrained. This preprocessing is required for the input images in the VGG19.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cd27b04",
   "metadata": {
    "id": "0cd27b04"
   },
   "outputs": [],
   "source": [
    "import torchvision.transforms as transforms\n",
    "from PIL import Image\n",
    "import glob\n",
    "import os\n",
    "import numpy as np\n",
    "\n",
    "def load_data_from_folder(N):\n",
    "    transform = transforms.Compose([\n",
    "    transforms.Resize(256),\n",
    "    transforms.ToTensor(),\n",
    "    transforms.Normalize(\n",
    "    mean=[0.485, 0.456, 0.406],\n",
    "    std=[0.229, 0.224, 0.225]\n",
    "    )])\n",
    "\n",
    "    images = []\n",
    "    labels = []\n",
    "    for f in glob.iglob(f'Numerosity_dataset/'+str(N)+'/*'):\n",
    "        labels.append(os.path.basename(f))\n",
    "        img = Image.open(f).convert('RGB')\n",
    "        images.append(np.asarray(transform(img)))\n",
    "\n",
    "    images = np.array(images)\n",
    "    images = torch.Tensor(images)\n",
    "    return images"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49f36641",
   "metadata": {
    "id": "49f36641"
   },
   "source": [
    "## Getting the activation from the final layer\n",
    "\n",
    "The below function provided would give the activation output from the final layer.\n",
    "\n",
    "We are adding a hook at the final layer to store the output. When a hook is added to a layer it will store the output of that layer for the latest forward pass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7b799b04",
   "metadata": {
    "id": "7b799b04"
   },
   "outputs": [],
   "source": [
    "def get_activation_classifier(images):\n",
    "    activation = {}\n",
    "\n",
    "    def getActivation(name):\n",
    "        def hook(model, input, output):\n",
    "            activation[name] = output.detach()\n",
    "        return hook\n",
    "\n",
    "    h = model.classifier[6].register_forward_hook(getActivation('linearlayer'))\n",
    "    out = model(images)\n",
    "    h.remove()\n",
    "    return torch.mean(activation['linearlayer'], 0).numpy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "633d9f47",
   "metadata": {
    "id": "633d9f47"
   },
   "source": [
    "## Calculate the VGG19 activation for numerosity 1-9\n",
    "\n",
    "In this section you will calculate the VGG19 activation for numerosities 1-9 from the images provided in the dataset. Getting activation for all the numbers would take around 5-10 mins.\n",
    "\n",
    "The activation vector of each numerosity should be added to the final activations list.\n",
    "\n",
    "Hint: Use the load_data_from_folder() and get_activation_classifier() function defined by us"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "17d14e61",
   "metadata": {
    "id": "17d14e61"
   },
   "outputs": [],
   "source": [
    "activations  = []\n",
    "# TODO append the activation for each numerosity to the activations list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "913149bb",
   "metadata": {},
   "source": [
    "## Printing average activation vector for each numerosity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "YVYcMddT7M1c",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "YVYcMddT7M1c",
    "outputId": "78ec51ad-b13e-4bd4-80dc-771dbf712081",
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for i in range(len(activations)):\n",
    "    print(\"Activation \" + str(i) + \" = \", activations[i][:5])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "770b4e19",
   "metadata": {},
   "source": [
    "You can check your values for acitvations for different numerosities.\n",
    "\n",
    "**Test:**\n",
    "\n",
    "Activation 0 =  \\[-1.6787993 -0.7309084 -1.0607507 -1.6177747 -1.4757576\\]  \n",
    "Activation 1 =  \\[-1.728821   -0.49921903 -0.61584103 -1.6838398  -0.10329571\\] \n",
    ". <br/>\n",
    ". <br/>\n",
    ". <br/>\n",
    "Activation 8 =  \\[-3.8989024  -1.7887913   1.4558771  -2.0705428   0.58432776\\]\n",
    "\n",
    "**Note: here activations\\[0\\] is the activation output for numerosity 1, activations\\[1\\] is for 2, and so on.**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a0f0096",
   "metadata": {
    "id": "6a0f0096"
   },
   "source": [
    "### Calculate cosine similarity between numerosity\n",
    "\n",
    "In this section you will create a 9x9 matrix showing the cosine similarity between activations of each numerosity\n",
    "\n",
    "You may use the [`sklearn.metrics.pairwise.cosine_similarity`](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html) function for creating 9x9 cosine similarity table"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "732e89e9",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 784
    },
    "id": "732e89e9",
    "outputId": "46b7a267-3558-440f-b088-41d1916dd333"
   },
   "outputs": [],
   "source": [
    "cosine_similarity_matrix = None\n",
    "\n",
    "# TODO: Calculate the cosine similarity between each numerosity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bf67250",
   "metadata": {},
   "source": [
    "## Visualizing the cosine similarity matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4081cb72",
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "\n",
    "xticklabels = yticklabels = range(1,10)\n",
    "ax = sns.heatmap(cosine_similarity_matrix, annot=True, linewidth=.5, yticklabels=yticklabels, xticklabels=xticklabels)\n",
    "ax.set_title(\"Cosine Similarity Between Numbers\")\n",
    "ax.set(xlabel=\"Number\", ylabel=\"Number\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebe4e1d1",
   "metadata": {
    "id": "ebe4e1d1"
   },
   "source": [
    "## Part 1: Distance Effect\n",
    "\n",
    "For the distance effect, you will plot the average cosine similarity (y) at each distance |n1 − n2| (x) and compute the correlation between these two variables. A distance effect will be signaled by a negative correlation close to r = −1.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4324ec86",
   "metadata": {
    "id": "4324ec86"
   },
   "source": [
    "## Get the X and Y list for distance effect and calculate correlation\n",
    "\n",
    "In this section, **you will create X and Y list for the distance effect and calculate correlation.** X is all possible distances between the numbers |n1 − n2| and Y is the average cosine similarity at that difference. For example, at difference X = 7, Y  would be avg(cosine(1, 8), cosine(2, 9)).  \n",
    "\n",
    "Calculate pearson correlation between distance vector (X) and average cosine similarity vector Y using [`scipy.stats.pearsonr`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pearsonr.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81288972",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "81288972",
    "outputId": "0707ace4-0e81-4e5f-b23a-dd63f968ed17"
   },
   "outputs": [],
   "source": [
    "#TODO calculate X and Y and pearsonr correlation between them"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51616e9c",
   "metadata": {
    "id": "51616e9c"
   },
   "source": [
    "### Plot the distance effect curve\n",
    "\n",
    "In this section, **you will plot the average cosine similarity (y) at each distance |n1 − n2| (x).**\n",
    "\n",
    "The cosine similarity matrix calculated in the previous section will help you quickly plot the distance effect curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5efa3ec8",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 544
    },
    "id": "5efa3ec8",
    "outputId": "fd7a0eb2-33b5-423c-c2bd-3928d6f47634"
   },
   "outputs": [],
   "source": [
    "#TODO: Write the code for plotting the distance effect curve"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "489baeee",
   "metadata": {},
   "source": [
    "## Below is the expected distance effect plot \n",
    "\n",
    "<img src=\"expected_distance_effect.png\" alt=\"Drawing\" style=\"width: 500px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15a128c9",
   "metadata": {
    "id": "15a128c9"
   },
   "source": [
    "## Fit a linear function over the above plotted curve and get the slope of the line\n",
    "\n",
    "In this section, we have provided code for fitting a linear function over the distance effect points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a2e0a957",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 501
    },
    "id": "a2e0a957",
    "outputId": "428e7ace-b0c7-4601-ae9f-08ca04f4291f"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.optimize import curve_fit\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Define the linear function\n",
    "def linear_func(x, a, b):\n",
    "    return a * x + b\n",
    "\n",
    "# Fit the linear function using curve_fit\n",
    "params, covariance = curve_fit(linear_func, X, Y)\n",
    "\n",
    "# Extract the fitted parameters\n",
    "a_fit, b_fit = params\n",
    "\n",
    "# Calculate the predicted values from the fitted function\n",
    "y_pred = linear_func(X, a_fit, b_fit)\n",
    "\n",
    "# Plot the data points\n",
    "plt.scatter(X, Y, label='Data Points', color='blue')\n",
    "\n",
    "# Plot the linear fit\n",
    "plt.plot(X, y_pred, label='Linear Fit', color='red')\n",
    "\n",
    "\n",
    "# Add labels and legend\n",
    "plt.xlabel('X')\n",
    "plt.ylabel('Y')\n",
    "plt.legend()\n",
    "\n",
    "# Show the plot\n",
    "plt.show()\n",
    "\n",
    "# Print the fitted parameters\n",
    "print(\"Fitted parameters:\")\n",
    "print(\"a =\", a_fit)\n",
    "print(\"b =\", b_fit)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d5fd16b",
   "metadata": {
    "id": "4d5fd16b"
   },
   "source": [
    "## Part 2 - Ratio Effect\n",
    "\n",
    "You will repeat the same process for the ratio effect, with the x-axis as the ratio $\\frac{max(n1, n2)}{min(n1, n2)}$ of the comparisons between each numerosity ($n1 \\neq n2$). Here, you will fit a negative exponential function to model the results; a ratio effect will be signaled by an R^2 value close to 1. The linking hypothesis is that the more similar the representations of two numbers, the slower they are compared.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a61946a",
   "metadata": {
    "id": "9a61946a"
   },
   "source": [
    "### Get the X and Y list for ratio effect and calculate correlation\n",
    "\n",
    "In this section, **you will create X and Y list for the ratio effect and calculate the correlation.** X is all possible ratios of the form $\\frac{max(n1, n2)}{min(n1, n2)}$ and Y is the cosine similarity at that ratio.   \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d850c0cc",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "d850c0cc",
    "outputId": "5735f21b-2cc6-461d-9ff5-0b7f3c761b13"
   },
   "outputs": [],
   "source": [
    "#TODO calculate X and Y and pearsonr correlation between them"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f41910ba",
   "metadata": {
    "id": "f41910ba"
   },
   "source": [
    "### Plot the ratio effect curve\n",
    "\n",
    "In this section, **you will plot the average cosine similarity (y) for each possible ratio $\\frac{max(n1, n2)}{min(n1, n2)}$ (x).**\n",
    "\n",
    "The cosine similarity matrix calculated in the previous section will again help you quickly plot the ratio effect curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "596c707a",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 472
    },
    "id": "596c707a",
    "outputId": "687e7cc6-081e-4c4a-ec27-12b1661955bc",
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#TODO: Write the code for plotting the ratio effect curve"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8c6b228",
   "metadata": {},
   "source": [
    "### Below is the expected ratio effect plot \n",
    "\n",
    "<img src=\"expected_ratio_effect.png\" alt=\"Drawing\" style=\"width: 500px;\"/>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "338586b8",
   "metadata": {
    "id": "338586b8"
   },
   "source": [
    "### Fit a negative exponential curve over the above ratio effect points\n",
    "\n",
    "In this section, we have provided code for fitting an exponential curve over the ratio effect points plotted in the above section.\n",
    "\n",
    "We have plotted the scatter points, fitted curve and reported the R^2 value for the fitted curve. R-squared value for a fitted curve can be calculated using the below given formula.\n",
    "\n",
    "$$R^2 = 1 - \\frac{\\sum_{i=1}^N (y_i - \\hat{y}_i)^2}{\\sum_{i=1}^N (y_i - \\bar{y})^2}$$, where $\\bar{y}$ is the mean of the y and $\\hat{y}_i$ is the predicted y."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cae77b5a",
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 465
    },
    "id": "cae77b5a",
    "outputId": "70feab22-1b52-49b7-f909-fda5a3431564"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.optimize import curve_fit\n",
    "\n",
    "# Fit the curve\n",
    "params, covariance = curve_fit(lambda t, a, b, c: a * np.exp(-b * t) + c, X, Y,maxfev=1000000)\n",
    "\n",
    "# Extract the fitted parameters\n",
    "a_fit, b_fit, c_fit = params\n",
    "\n",
    "# Calculate the predicted values from the fitted curve\n",
    "y_pred = a_fit * np.exp(np.array(X)* - b_fit) + c_fit\n",
    "\n",
    "# Calculate the mean of the actual data points\n",
    "y_mean = np.mean(Y)\n",
    "\n",
    "# Calculate R^2\n",
    "ss_res = np.sum((Y - y_pred)**2)\n",
    "ss_tot = np.sum((Y - y_mean)**2)\n",
    "r_squared = 1 - (ss_res / ss_tot)\n",
    "\n",
    "print(\"R-squared value =\", r_squared)\n",
    "\n",
    "# Plot the data points\n",
    "plt.scatter(X, Y, label='Data Points', color='blue')\n",
    "\n",
    "# Plot the fitted curve\n",
    "x_fit = np.linspace(min(X), max(X), 100)  # Generate points for the fitted curve\n",
    "y_fit = a_fit * np.exp(np.array(x_fit)* - b_fit) + c_fit\n",
    "plt.plot(x_fit, y_fit, label='Fitted Curve', color='red')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a6c9101",
   "metadata": {
    "id": "f6f9a7cf"
   },
   "source": [
    "## Questions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14a06a25",
   "metadata": {},
   "source": [
    "Respond to each question in one or two paragraphs. Remember, we are looking for connections to psychological plausibility in humans.\n",
    "\n",
    "(1) You computed the fit of the model to the distance effect and to the size effect. How would you interpret the r (correlation) and R^2 values that you obtained?  \n",
    "\n",
    "(2) You have modeled numerosity processing using CNN models. Imagine extending this to model the development of numerosity. You would find checkpointed CNN models and use the stimuli to document how their numerosity representations change over training. Do you think the model would mimic the developmental progression shown by children? \n",
    "\n",
    "\n",
    "<img src=\"children_distance_effect.png\" alt=\"Drawing\" style=\"width: 500px;\"/>\n",
    "\n",
    "(3) You have examined the activations at the final fully connected layer. Would you expect to see the same results at other layers, and why? \n",
    "\n",
    "(4) Why do you think that Vision models trained on image classification are sensitive to numerical information? \n",
    "\n",
    "(5) This exercise shows the alignment between humans and CNNs for numerosity understanding. The importance of this is clear to cognitive science. What is the value, if any, of these findings for computer vision research? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ad9618a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
